'use strict';

const fs = require('fs');

// Read and transform data
fs.readFile('practica.json', (err, data) => {
  if (err) throw err;
  let featureCollection = JSON.parse(data);
  const properties = featureCollection.features[0].properties;
  featureCollection.features.forEach( feature => {
      for (let i=0; i < feature.properties.avgbedrooms.length; i++ ) {
        feature.properties.avgbedrooms[i].bedrooms = i;
      }
  });
  // Write data
  let newFeatureCollection = JSON.stringify(featureCollection, null, 2);
  fs.writeFileSync('airbnb-madrid-neighbourhood.json', newFeatureCollection, (err) => {
    if (err) throw err;
    console.log('Data written to file');
  });
});

