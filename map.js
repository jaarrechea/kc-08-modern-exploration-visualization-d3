// Constants
const windowHeight = window.innerHeight;

// Constants for map
const mapMarginLeft = 40;
const mapMarginRight = 40;
const titleHeight = 90;
const mapMarginTop = 50;
const numberOfLegends = 6;
const legendSeparation = 2;
const legendHeight = 15;
const legendPosY = 30;
const tooltipInTransitionDuration = 200;
const tooltipOutTransitionDuration = 500;

// Constants for bedroom bars
const barMarginLeft = 40;
const barMarginRight = 40;
const barMarginTop = 20;
const barMarginBottom = 45;
const barTransitionDuration = 200;
const barColor = "#ff681f";
const barTextColor = "#ff681f";
// const barTextColor = "#000000";
const paddingBetweenBars = 0.2;
const textBarBorderTop = -1 ;
const singularTextProperty = "apart.";
const pluralTextProperty = "aparts.";
const barTitleTop = 15;
const barLegendXAxis = "N&#186; Habitaciones";
const barLegendYAxis = "N&#186; Apartamentos";
// Globals
let maxPrice = 200;
let repaintAll = true;
let tooltip = d3.select("body").append("div")
  .attr("class", "tooltip")
  .style("opacity", 0);

const urls = [];
urls[0] = 'https://gist.githubusercontent.com/miguepiscy/2d431ec3bc101ef62ff8ddd0e476177f/raw/2482274db871e60195b7196c602700226bdd3a44/practica.json';
urls[1] = 'https://gist.githubusercontent.com/jaarrechea/a835e25fc328037daedd5fc1a93d1e23/raw/4ae5d91927f591f3cd10f10f1d9efc379831a81c/airbnb-madrid-neighbourhood.json';
urls[2] = 'https://gist.githubusercontent.com/jaarrechea/a19bde6045b06e4101abe4815627867e/raw/3ebdcd0ad508455350cc9e78fba95f8325299ee5/airbnb-madrid-neighbourhood-real.json';
urls[3] = 'https://gist.githubusercontent.com/jaarrechea/f368620e9418524a54d5fea87aeb9f05/raw/f19d1155151ec65ae803ff4ac8a1eb9f0d592443/airbnb-madrid-distrites-real.json';

let currentUrl = urls[0];
paintAll();
// Load data from a json 
function paintAll() {
  if (repaintAll) {
    d3.json(currentUrl)
    .then((featureCollection) => {
      drawCharts(featureCollection);
    });
  }
  repaintAll = true;
}

function changeUrl() {
  const e = document.getElementById("selectURL"); 
  const index = e.options[e.selectedIndex].value;
  currentUrl = urls[index];
  paintAll();
}

/* ************************* */
/* Functions for drawing map */
/* ************************* */
function drawCharts(featureCollection) {
  maxPrice = d3.max(featureCollection.features, d => d.properties.avgprice);

  const map = drawMercator(featureCollection, maxPrice);
  lengends(map.svg, maxPrice);
  drawBedroomBars(map.neighbourhoodPath);
  map.svg.on('click', paintAll);

}

function drawMercator(featureCollection, maxPrice) {
  // Refresh
  d3.select('.svgMap').remove();
  d3.select('.svgBedroomBars').remove();

  // Paint charts
  const divMap = d3.select('#map')
  const svg = divMap.append('svg').attr('class', 'svgMap');

  const width = divMap.style('width').slice(0, -2);
  const height = windowHeight;

  svg.attr('width', width)
    .attr('height', height);

  const center = d3.geoCentroid(featureCollection);
  const projection = d3.geoMercator()
    .fitSize([width - (mapMarginLeft + mapMarginRight), height - mapMarginTop - titleHeight - legendPosY], featureCollection)
    .center(center)
    .translate([width / 2 + mapMarginLeft + mapMarginRight, height / 2 ])

  const pathProjection = d3.geoPath().projection(projection);

  const features = featureCollection.features;

  const neighbourhoodPath = svg.selectAll('.neighbourhood')
    .data(features)
    .enter()
    .append('path')
    .attr("style", "stroke-width:0.1;stroke:#FFFFFF;")
    .attr('class', function (d) {
      return 'neighbourhood ' + d.properties.name.replace(/ /g,"");
    });

  // Fill color depending on average price.
  neighbourhoodPath
    .attr('d', pathProjection)
    .attr('fill', fillColor)
    .on("mouseout", () => {
      tooltip.transition()
        .duration(tooltipOutTransitionDuration)
        .style("opacity", 0);
      d3.selectAll(".neighbourhood").attr("style", "stroke-width:0.1;stroke:#FFFFFF;");
    })
    .on("mousemove", function (d) {
      // data to be shown in tooltip
      const name = d.properties.name;
      const price = (d.properties.avgprice || 0).toString().replace(/\./g,",");

      // Place text in the middle of the rectangle
      tooltip.html(`${name}</br>  ${price}€`)
        .style("left", (d3.event.pageX - 88) + "px")
        .style("top", (d3.event.pageY - 40) + "px")
        .style("height", "32px");
      tooltip.transition()
        .duration(tooltipInTransitionDuration)
        .style("opacity", .9);

      d3.selectAll(".neighbourhood").attr("style", "stroke-width:0.1;stroke:#FFFFFF;");
      d3.selectAll(".neighbourhood." + name.replace(/ /g,"")).attr("style", "stroke-width:2;stroke:#000;");

    })

  return { "svg": svg, "neighbourhoodPath": neighbourhoodPath };


}

// Fill color. The more expensive, the redder; the more cheap, the greener. Yellow is used for price in the middle.
function fillColor(neighbourhoodPath) {
  const price = neighbourhoodPath.properties.avgprice || 0;
  return d3.interpolateRdYlGn(1 - (price / maxPrice));
}

function lengends(svg, maxPrice) {
  const width = svg.style('width').slice(0, -2);

  const legendBox = svg
    .append('g');

  const scaleLegend = d3.scaleLinear()
    .domain([0, numberOfLegends])
    .range([0, width]);

  const legendWidth = (width / numberOfLegends) - legendSeparation;
  for (let index = 0; index < numberOfLegends; index++) {
    const posX = scaleLegend(index);
    const legendGroup = legendBox
      .append('g')
      .attr('transform', `translate(${posX}, 0)`);

    // ratio or percent
    const ratio = index / (numberOfLegends - 1);
    // Add legend with a colored square and a text
    const legendRect = legendGroup
      .append('rect');
    const legendText = legendGroup
      .append('text');

    // Legend color
    legendRect
      .attr('width', legendWidth)
      .attr('height', legendHeight)
      .attr('fill', d3.interpolateRdYlGn(1 - ratio));

    // Legend text
    const limitPrice = Number(maxPrice * ratio).toLocaleString('es',  {minimumFractionDigits: 0, maximumFractionDigits: 2 }) + " €/m2";
    legendText
      .attr('font-size', '14px')
      .attr('font-weight', 'bold')
      .text(limitPrice)
      .attr('transform', function (d) {
        const textLength = this.getComputedTextLength();
        const center = legendWidth / 2;
        return `translate(${center - (textLength / 2)}, ${legendPosY})`;
      });
  }
}

/* ********************************** */
/* Functions for drawing Bedroom Bars */
/* ********************************** */
function drawBedroomBars(neighbourhoodPath) {
  const divBedroomBars = d3.select('#bedroomBars')
  const svg = divBedroomBars.append('svg').attr('class', 'svgBedroomBars');

  const width = divBedroomBars.style('width').slice(0, -2) - 20;
  const height = windowHeight;

  svg.attr('width', width)
    .attr('height', height);

  //  https://observablehq.com/@d3/d3-scaleband
  // We use scaleBand for proportional widths
  const scaleX = d3.scaleBand()
    .range([barMarginLeft, width - barMarginRight])
    .padding(paddingBetweenBars); // space between columns

  const scaleY = d3.scaleLinear()
    .range([height - titleHeight - barMarginTop, barMarginTop ]);

  // Initialize axis
  const xAxis = d3.axisBottom(scaleX);
  const yAxis = d3.axisLeft(scaleY);

  const groupXAxis = svg.append('g');
  groupXAxis
    .attr('transform', `translate(0, ${height - titleHeight - barMarginTop})`)
    .attr('class', 'barXAxis')
    .call(xAxis);
  svg  
    .append("text")
    .attr("class", "labelBedrooms")
    .attr("x", `${width - barMarginRight}`)
    .attr("y", `${height - titleHeight - barMarginTop + 18}`)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .attr('font-size', '14px')
    .html(barLegendXAxis);

  const groupYAxis = svg.append('g');
  groupYAxis
    .attr('transform', `translate(${barMarginLeft}, 0)`)
    .attr('class', 'barYAxis')
    .call(yAxis);
  svg  
    .append("text")
    .attr("class", "labelApartments")
    .attr("transform", "rotate(-90)")
    .attr("x", -barMarginTop)
    .attr("y", 0)
    .attr("dy", ".71em")
    .style("text-anchor", "end")
    .attr('font-size', '14px')
    .html(barLegendYAxis);

  const xAxisWidth = width - barMarginRight - barMarginLeft;
  d3.selectAll("g.barYAxis .tick")
    .append("line")
    .classed("barYAxisline", true)
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("y2", 0)
    .attr("x2", xAxisWidth);

  const axis = {"xAxis": xAxis, "yAxis": yAxis} 

  // Active click event on a neighbourhood path
  neighbourhoodPath
    .on("click", d =>
      paintBedroomBars(d.properties, svg, scaleX, scaleY, axis, height, xAxisWidth));

}

function paintBedroomBars(properties, svg, scaleX, scaleY, axis, height, xAxisWidth) {
  const avgbedrooms = properties.avgbedrooms;
  const name = properties.name;
  repaintAll = false;

  d3.selectAll(".neighbourhood").attr('fill', fillColor);
  d3.selectAll(".neighbourhood." + name.replace(/ /g,"")).attr('fill', barColor);

  svg.selectAll('.barTitle').remove();
  const title = svg
    .append("text")
    .text(name)
    .attr('x', function(d) {
      const textLength = this.getComputedTextLength();
      return (xAxisWidth - textLength) / 2;
    })
    .attr('y', barTitleTop - 5)
    .attr('class', 'barTitle');
  
  title
    .transition().duration(barTransitionDuration)
    .attr('font-size', '14px');

  // Axis
  scaleX.domain(avgbedrooms.map(d => d.bedrooms))
  scaleY.domain([0, d3.max(avgbedrooms, d => d.total)]).nice()

  svg.selectAll(".barXAxis")
    .transition()
    .duration(barTransitionDuration)
    .call(axis.xAxis);

  d3.selectAll("g.barYAxis .tick").remove();
  svg.selectAll(".barYAxis")
    .transition()
    .duration(barTransitionDuration)
    .call(axis.yAxis)

  // Repaint bars
  svg.selectAll('.bar').remove();
  d3.selectAll("g.barYAxis .tick")
    .append("line")
    .classed("barYAxisline", true)
    .attr("stroke", "currentColor")
    .attr("x1", 0)
    .attr("y1", 0)
    .attr("y2", 0)
    .attr("x2", xAxisWidth);
  const barGroup = svg
    .selectAll('.bar')
    .data(avgbedrooms)
    .enter()
    .append('g')
    .attr('class', 'bar');

  // Bar's attributes
  const rect = barGroup
    .append('rect')
    .attr('x', d => scaleX(d.bedrooms))
    .attr('y', height - barMarginBottom)
    .attr('width', scaleX.bandwidth())
    .attr('fill', barColor);
  
  // Bar transition
  rect
    .transition().duration(barTransitionDuration)
    .attr('y', d => scaleY(d.total))
    .attr('height', d => scaleY(0) - scaleY(d.total))

  // Add text displaying number of apartments
  const text = barGroup
  .append("text")
  .text((d) => {
    if (d.total == 1) {
      return "1 " + singularTextProperty;
    } else {
      return d.total + " " + pluralTextProperty;
    }
  })
  .attr('x', function(d) {
    return scaleX(d.bedrooms);
  })
  .attr('y', height)
  .attr('fill', barTextColor);
  
text
  .transition().duration(barTransitionDuration)
  .attr('font-size', '10px')
  .attr('x', function(d) {
    const textLength = this.getComputedTextLength();
    return scaleX(d.bedrooms) + ((scaleX.bandwidth()- textLength) / 2) + 11;
  })
  .attr('y', (d) => {
    const y = scaleY(d.total) + textBarBorderTop;
    return y > height ? height : y;
  })

}
