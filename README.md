# Práctica Exploración y visualización de datos

En este repositorio se incluyen los archivos para resolver la práctica cuyo enunciado se encuentra en el [Practica.md](Practica.md).

## Solución propuesta

Se ha resuelto la práctica, tal y como se requería, mostrando un mapa con los barrios de la ciudad de Madrid y pintándolos por colores según el precio medio del alquiler en el barrio y al pinchar en cada uno de ellos se muestra una gráfica de barras en la que en el eje Y tiene el número de propiedades y en el eje X el número de habitaciones.

En la ventana se permite elegir cuatro fuentes de datos distintas:

- Datos proporcionados en la práctica (ver el primer de la sección de Fuentes de datos), cuya información es ficticia.
- Datos obtenidos tras la corrección del archivo del punto anterior.
- Datos por barrio, pero utilizando la fuente real de Airbnb.
- Datos por distrito utilizando la fuente real de Airbnb.

## Fuentes de datos

Los archivos que se van a utilizar como fuentes de datos para esta práctica son los siguientes:

- Inicialmente, se recibió este [enlace](https://gist.githubusercontent.com/miguepiscy/2d431ec3bc101ef62ff8ddd0e476177f/raw/2482274db871e60195b7196c602700226bdd3a44/practica.json) con datos ficticios de los precios y número total de viviendas según número de habitaciones.

- Datos principales de Airbnb, que se obtienen de [aquí](https://public.opendatasoft.com/explore/dataset/airbnb-listings/export/?disjunctive.host_verifications&disjunctive.amenities&disjunctive.features&q=Madrid&dataChart=eyJxdWVyaWVzIjpbeyJjaGFydHMiOlt7InR5cGUiOiJjb2x1bW4iLCJmdW5jIjoiQ09VTlQiLCJ5QXhpcyI6Imhvc3RfbGlzdGluZ3NfY291bnQiLCJzY2llbnRpZmljRGlzcGxheSI6dHJ1ZSwiY29sb3IiOiJyYW5nZS1jdXN0b20ifV0sInhBeGlzIjoiY2l0eSIsIm1heHBvaW50cyI6IiIsInRpbWVzY2FsZSI6IiIsInNvcnQiOiIiLCJzZXJpZXNCcmVha2Rvd24iOiJyb29tX3R5cGUiLCJjb25maWciOnsiZGF0YXNldCI6ImFpcmJuYi1saXN0aW5ncyIsIm9wdGlvbnMiOnsiZGlzanVuY3RpdmUuaG9zdF92ZXJpZmljYXRpb25zIjp0cnVlLCJkaXNqdW5jdGl2ZS5hbWVuaXRpZXMiOnRydWUsImRpc2p1bmN0aXZlLmZlYXR1cmVzIjp0cnVlfX19XSwidGltZXNjYWxlIjoiIiwiZGlzcGxheUxlZ2VuZCI6dHJ1ZSwiYWxpZ25Nb250aCI6dHJ1ZX0%3D&location=16,41.38377,2.15774&basemap=jawg.streets).
Los datos obtenidos de esta fuente están en el archivo *airbnb-listings Madrid.csv*, que se encuentra bajo el directorio *data-source*.

- Para poder pintar el mapa de Madrid por distritos se ha recurrido a la página web de datos de Madrid, concretamente a [ésta](https://datos.madrid.es/portal/site/egob/menuitem.c05c1f754a33a9fbe4b2e4b284f1a5a0/?vgnextoid=46b55cde99be2410VgnVCM1000000b205a0aRCRD&vgnextchannel=374512b9ace9f310VgnVCM100000171f5a0aRCRD&vgnextfmt=default). En esta página se ha bajado el archivo "Distritos en formato geográfico", *DISTRITOS_ETRS89.zip*, y se encuentra bajo el directorio *data-source*.

- El archivo json que corrige los datos recibidos en la práctica se ha publicado [aquí](https://gist.githubusercontent.com/jaarrechea/a835e25fc328037daedd5fc1a93d1e23/raw/4ae5d91927f591f3cd10f10f1d9efc379831a81c/airbnb-madrid-neighbourhood.json).

- El archivo json que proporciona los datos reales de Airbnb por barrios se ha publicado [aquí](https://gist.githubusercontent.com/jaarrechea/a19bde6045b06e4101abe4815627867e/raw/3ebdcd0ad508455350cc9e78fba95f8325299ee5/airbnb-madrid-neighbourhood-real.json).

- El archivo json que proporciona los datos reales de Airbnb por distrito se ha publicado [aquí](https://gist.githubusercontent.com/jaarrechea/f368620e9418524a54d5fea87aeb9f05/raw/f19d1155151ec65ae803ff4ac8a1eb9f0d592443/airbnb-madrid-distrites-real.json).

## Entregables

Se entregan lo siguientes recursos:

- map.html: Este es el archivo que debe ser abierto en un navegador. Presenta en la parte superior izquierda un combo donde se puede elegir con qué datos se quieren ver las gráficas
- map.js: Archivo donde se encuentra todo el código que genera las gráficas.
- map.css: estilos principales utilizados en la página map.html.

## Directorios

Para generar los archivos fuentes adicionales se han realizado unos programas en javascript, ejecutables con node (es necesario tener instalado node), que se describen a continuación:

- neighbourhood-fake: el objetivo de este directorio es crear un archivo similar al entregado al de la práctica, pero con datos corregidos.
  - practica.json: es el archivo original proporcionado que contiene errores en la entrada feature.properties.avgbedrooms. Dentro de esta sección se observa que el campo bedrooms solo tienen los valores 0 y 1, cuando debería tener  los valores 0, 1, 2, 3 y 4.
  - fix-data.js: programa javascript que lee el archivo *practica.json* y genera una copia del mismo, *airbnb-madrid-neigbourhood.json*, pero con los datos corregidos. Este programa se ejecuta con ```node fix-data.js```.
  - airbnb-madrid-neighbourhood.json: archivo igual que *practica.json*, pero con los datos corregidos. Este archivo se ha subido a un gist de github [aquí](https://gist.githubusercontent.com/jaarrechea/a835e25fc328037daedd5fc1a93d1e23/raw/4ae5d91927f591f3cd10f10f1d9efc379831a81c/airbnb-madrid-neighbourhood.json).
- neighbourhood-real: el objetivo de este diretcorio es crear un archivo con datos reales de Airbnb de cada barrio de Madrid, utilizando las coordenadas del mapa del archivo entregado para la práctica.
  - practica.json: es el archivo original proporcionado para la práctica. En este caso se utiliza para leer las coordenadas que sirven para pintar los barrios de Madrid.
  - source-neighbourhood.csv: archivo que contiene los datos reales de cada barrio. Se ha obtenido del archivo *airbnb-listings Madrid.csv* mediante *HabitacionesBarriosDistritos.twbx*, que está en el directorio data-source, pero podría haberse obtenido utilizando R, Python (con pandas) o cualquier otro sistema. Se ha utilizado Tableau porque es lo que se ha utilizado hasta ahora en el Bootcamp. En próximos módulos se estudiará R y Python.
  - neighbourhood.xlsx: libro excel en el que se ha importado *source-neighbourhood.csv* y mediante una tabla dinámica se ha obtenido el siguiente archivo.
  - neighbourhood-real.csv: archivo con los datos reales de Airbnb consolidados por cada barrio de Madrid.
  - build-json.js: es el programa javascript, que ejecutado con ```node build-json.js```, procesa el archivo .csv anterior junto con *practica.json* y genera el archivo *airbnb-madrid-neighbourhood-real.json*, que ha sido publicado en un gist de github [aquí](https://gist.githubusercontent.com/jaarrechea/a19bde6045b06e4101abe4815627867e/raw/3ebdcd0ad508455350cc9e78fba95f8325299ee5/airbnb-madrid-neighbourhood-real.json). Para ejecutar este programa es necesario tener instalado el módulo *csv-parser* con ```npm install csv-parser```. Al instalar este módulo se crea una carpeta de dependencia node_modules y el archivo *package-lock.json*.
- distrite: el objetivo de este directorio es crear un archivo .json con los datos reales de Airbnb de cada distrito de Madrid.
  - SHP_ETRS89: esta carpeta contiene los archivos descomprimidos de *DISTRITOS_ETRS89.zip*, que está bajo *data-source*. Dentro de esta carpeta se ha ejecutado ```ogr2ogr -f GeoJSON -t_srs crs:84 distrites.geojson DISTRITOS.shp``` con el objeto de obtener un archivo con las coordenadas del mapa de los distritos de Madrid, que pueda ser pintado con geomercator de d3.js. Para poder ejecutar ogr2ogr es necesario tener instalado previamente gdal, que en un Mac se consigue con ```brew install gdal```. La información se ha obtenido [aquí](https://ben.balter.com/2013/06/26/how-to-convert-shapefiles-to-geojson-for-use-on-github/).
  - distrites.geojson: es el archivo con las coordenadas de los distritos de Madrid, obtenido en el anterior punto.
  - source-distrites.csv: archivo que contiene la información detallada por distrito y número de habitaciones por vivienda de Airbnb, en Madrid, que se obtiene a patri del archivo *airbnb-listings Madrid.csv* mediante *HabitacionesBarriosDistritos.twbx*, que está en el directorio data-source. Este archivo csv también se podría haber obtenido con R o con Python, utilizando pandas.
  - distrites.xlsx: libro excel en el que se ha importado *source-distrites.csv* y mediante una tabla dinámica se ha obtenido el siguiente archivo.
  - distrite-real.csv: archivo con los datos reales de Airbnb consolidados por cada distrito de Madrid.
  - build-json.js: es el programa javascrip, que ejecutado con ```node build-json.js```, procesa el archivo .csv anterior y genera el archivo *airbnb-madrid-distrites-real.json*, que ha sido publicado en un gist de github [aquí](https://gist.githubusercontent.com/jaarrechea/f368620e9418524a54d5fea87aeb9f05/raw/f19d1155151ec65ae803ff4ac8a1eb9f0d592443/airbnb-madrid-distrites-real.json). Para ejecutar este programa es necesario tener instalado el módulo *csv-parser* con ```npm install csv-parser```. Al instalar este módulo se crea una carpeta de dependencia node_modules y el archivo *package-lock.json*.


## Vídeo explicativo

Se ha creado un vídeo en el que se explica el funcionamiento general de la página *map.html*, cómo se ha codificado *map.js* y cómo se han obtenido cada uno de los archivos que se han utilizado.

El vídeo, graficos-d3-airbnb-madrid.mp4, se puede bajar de este [enlace](https://we.tl/t-0UKKOGW8ow). Este enlace caduca el 14-03-2020.
