'use strict';

// Install csv-parser -> npm install -g csv-parser

const fs = require('fs');
const csv = require('csv-parser');

const beedrooms = {};
fs.createReadStream('neighbourhood-real.csv')
.pipe(csv({ separator: '\t' }))
.on('data', function(data) {
  const avgbedrooms = [];
  for (let i = 0; i <= 10; i++) {
    let value = data[i];
    if (value === undefined || value === null || value === '') {
      value = 0;
    } 
    avgbedrooms.push({"bedrooms":i, "total": parseInt(value)})
  }
  beedrooms[data['name']] = {"avgprice": parseFloat(data['avgprice'].replace(/,/g, '.')), "avgbedrooms": avgbedrooms}

})
.on('end',function(){
  // Read and transform data
  fs.readFile('practica.json', (err, data) => {
    if (err) throw err;
    let featureCollection = JSON.parse(data);
    featureCollection.features.forEach( feature => {
      const data = beedrooms[feature.properties.name];
      if (data === undefined || data === null) {
        console.log(feature.properties.name + ' was not found');
      } else {
        feature.properties.avgprice = data.avgprice;
        feature.properties.avgbedrooms = data.avgbedrooms;
        delete feature.properties.properties;
      }
    });
    // Write data
    let newFeatureCollection = JSON.stringify(featureCollection, null, 2);
    fs.writeFileSync('airbnb-madrid-neighbourhood-real.json', newFeatureCollection, (err) => {
      if (err) throw err;
      console.log('No data written to file');
    });
  });
});


// // Read and transform data
// fs.readFile('practica.json', (err, data) => {
//   if (err) throw err;
//   let featureCollection = JSON.parse(data);
//   const properties = featureCollection.features[0].properties;
//   featureCollection.features.forEach( feature => {
//       for (let i=0; i < feature.properties.avgbedrooms.length; i++ ) {
//         feature.properties.avgbedrooms[i].bedrooms = i;
//       }
//   });
//   // Write data
//   let newFeatureCollection = JSON.stringify(featureCollection, null, 2);
//   fs.writeFileSync('airbnb-madrid-neigbourhood.json', newFeatureCollection, (err) => {
//     if (err) throw err;
//     console.log('Data written to file');
//   });
// });

